Source: python-livereload
Section: python
Priority: optional
Maintainer: Agustin Henze <tin@debian.org>
Uploaders: Pierre-Elliott Bécue <peb@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               furo <!nodoc>,
               help2man,
               python3-all,
               python3-myst-parser <!nodoc>,
               python3-setuptools,
               python3-sphinx <!nodoc>,
               python3-sphinxcontrib.programoutput <!nodoc>,
               python3-tornado
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://github.com/lepture/python-livereload
Vcs-Git: https://salsa.debian.org/debian/python-livereload.git
Vcs-Browser: https://salsa.debian.org/debian/python-livereload

Package: python-livereload-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Recommends: python3-livereload
Multi-Arch: foreign
Description: automatic browser refresher (documentation)
 It is really boring for Web developers to need to refresh their browser
 every time they save a (CSS, JavaScript, or HTML) file. LiveReload will
 take care of that for you, so that when you save a file, your browser
 will refresh itself - and what's more, it can perform tasks such as
 compiling LESS to CSS before the browser reload.
 .
 This package contains API documentation and examples.

Package: python3-livereload
Architecture: all
Depends: python3-tornado,
         ${misc:Depends},
         ${python3:Depends}
Recommends: python3-pyinotify
Suggests: coffeescript,
          node-less,
          node-uglify,
          python-livereload-doc,
          python3-django,
          python3-flask,
          python3-slimmer
Description: automatic browser refresher (Python 3)
 It is really boring for Web developers to need to refresh their browser
 every time they save a (CSS, JavaScript, or HTML) file. LiveReload will
 take care of that for you, so that when you save a file, your browser
 will refresh itself - and what's more, it can perform tasks such as
 compiling LESS to CSS before the browser reload.
 .
 This package contains the Python 3 version of livereload.
